---
title: "Exam for Data Analysis"
output: html_notebook
---

```{R fig3}
success <- 0:20
plot(success, dpois(success, lambda=8),
     type ='h',
     main ='Poisson Distribution (lambda=2)',
     ylab ='Probability',
     xlab ='Successes (k)',
     lwd=3, col="blue")
par(new=TRUE)
plot(success, ppois(success, lambda=5), type = 'l', ylab="", xlab="", lty=1, lwd=2, col="red", axes = FALSE)
axis(4, ylim=c(0,1), col="black", las=1)
legend("topright", legend=c("cumulative P"), col=c("red"), lty=1, cex=0.8)
```



```{R}
## poisson distribution
lambda <- 8 # 2 at 500, 8 at 2000
k <- 3:2000 # 3 twin births or more
sum(dpois(k, lambda))

```


## Task 2 a

```{r}
# Doing a z-transformation
mu = 3250
sigma = 1230
x = 2400
z = (x - mu) / sigma
z # -0.6910569

```

## Task 2 b
```{r}
mu = 3250
sigma = 1230

pnorm(4200, mean=mean, sd=sigma) - pnorm(2400, mean=mu, sd=sigma)
```
## Task 2 c
```{r}
mu = 3250
sigma = 1230
p = 0.75

qnorm(p, mean=mu, sd=sigma)
```
## Task 3 a
```{r}
DataB1 <- read.delim("./data/DataB1.txt")
DataB1
```



```{R}
hist(DataB1$Nmin, xlab="Soil Nmin",
     main="Distribution of Nmin", ylab="Density",
     freq=FALSE)
density <- density(DataB1$Nmin)
lines(density, col="blue")
```
```{r}
max(DataB1$Nmin)
```

## task 3 b - Test if data is normally distributed
```{r}
## first show qqnorm
qqnorm(DataB1$Nmin)
qqline(DataB1$Nmin, col=c("red"))

```
```{r}
## more robust test
shapiro.test(DataB1$Nmin)
```
## 3d
```{r}

DataB1_subset = subset(DataB1, soil == "D2a" | soil == "D5a")
# convert to factor and make sure we only have the two factors we care about
DataB1_subset$soil <- as.factor(as.character(DataB1_subset$soil))


res = boxplot(Nmin ~ soil, data=DataB1_subset, col=c("blue", "green"))
legend("topright", title="Soil Type", legend = levels(DataB1_subset$soil), fill = c("blue", "green"))


res
```
## Task 4

### normality
```{r}
DataB1_D2a = subset(DataB1, soil == "D2a")

shapiro.test(DataB1_D2a$Nmin)

```
### simple t-test
```{r}

t.test(DataB1_D2a$Nmin, mu=6.8)
```

## 4d
```{r}
DataB1_D2a = subset(DataB1, soil == "D2a")
DataB1_D5a = subset(DataB1, soil == "D5a")

t.test(DataB1_D2a$Nmin, DataB1_D4a$Nmin, paired = FALSE, alternative = c("two.sided"))

```


## Task 7
New Dataset!!
```{r}
DataB2 <- read.delim("./data/DataB2.txt")
DataB2
```

```{r}
shapiro.test(subset(DataB2, soil== "D3a")$Nmin)
shapiro.test(subset(DataB2, soil== "D5a")$Nmin)
shapiro.test(subset(DataB2, soil== "D5b")$Nmin)
```


## apply leveneTest
```{r}
library(car)
```
```{r}
leveneTest(Nmin ~ soil, data=DataB2)

```
```{r}
res.aov <- aov(Nmin ~ soil, data = DataB2)
summary(res.aov)
```
```{r}
TukeyHSD(res.aov, conf.level = 0.99)
```
## Task 7 a)

```{r}
trees <- read.delim("data/projectdata.tsv") ## make sure this file is available
pine <- subset(trees, Species == "Common pine")
#head(pine)
#
cor(pine$DBH, pine$Height, method = c("spearman"))

```
```{r}
linear_model <- lm(Height ~ DBH, data = pine)
summary(linear_model)
```

```{r}
x <- seq(min(pine$DBH), max(pine$DBH), by = 0.1)
predict <- coef(linear_model)[1] + coef(linear_model)[2]*x

plot(pine$DBH, pine$Height, ylab="Height [m]", xlab="Diameter [cm]")
lines(x, predict, col="red")
abline(h=c(0,10,20,30), lty="dashed", col="grey")
abline(v=c(10,20,30,40,50), lty="dashed", col="grey")
```

