# Course Material - Environmental spatial data analysis

## Seminar 1 - 6
There are notebooks for the individual seminars.



## Group Work
the Notebook is in "group_work.Rmd". The data is not in the repo since of its unclear copyright status.

## build a nice pdf presentation from the R Notebook
see: https://jlintusaari.net/how-to-compile-r-markdown-documents-using-docker/


## generate the pdf from the file 'group_work.Rmd'
You will need to have docker installed and change the path to your folder with the data. In the header of the Notebook you will need to change some things. So this not a one command instant working solution.
The resulting pdf will have some problems with diagrams, which are outside of the page.
```
## beware the data is outside this repo
### So inside the container it assumes to open /data/projectdata.tsv
### you have to change /home/christian/Insy... to your path
docker run --rm -v $PWD:/report -v "/home/christian/Insync/christian.winkelmann@gmail.com/Google Drive/documents/Studium/FIT/courses/Environmental spatial data analysis - Wallor M3/projectwork/:/data" -w /report rocker/verse:3.5.1  Rscript --vanilla render_group_work.R
```