---
title: "Sample size calculation"
output: html_notebook
---
This notebook is intended to support students in the course *Environmental spatial data analysis*. The aim of this practical seminar is:

+ to transfer equations into R, and
+ to calculate sample sizes (for simple random samples) in order to guarantee a certain precision of sampling results at a certain statistical significance level.

**Data sources**: No data required.

**Remark**: Everybody works on her/his own directory.

## Exercises
### Exercise 1
From previous investigations it is known that the heights of *Douglas Fir* trees are normally distributed with $\mu$ = 36 m and $\sigma^2$ = 5.76 $m^2$. In a certain *Douglas Fir* growing area it is to be determined the average tree height with a simple random sampling approach.

**How many trees must be randomly selected to estimate the mean height with +/- 1 m precision at the 5% error level?**

The formular is: $n=z_{\alpha/2}^2*\sigma^2/d^2$

Define values for $z$, $\sigma^2$, and $d$ and transfer the formular to R to calculate $n$!

```{r}
get_n <- function(z, sigma_sq, d) {
  n <- z**2 * sigma_sq / d**2 
  return(n)
}

## getting z is a bit strange, because you have a deviation both end. To get 95% https://www.mathsisfun.com/data/standard-normal-distribution-table.html accuracy you have divide it by 2, get 47.5% and for this you can read the z-value of 1.96
# This is basically the z transformation

get_n(z = 1.96, sigma_sq = 5.76, d=0.5)


```
for d=1 - 23 trees. Round up!! not down
for d=0.5 -> 88.51046    -> 89 trees. Round up!!

### Exercise 2
In a grassland area a sampling approach to monitor the change of herb species is to be developed. The determination of the number of species is accomplished at observation plots according to a certain mapping rule. From previous investigations it is known that the number of herbs (herb species) at grassland is normally distributed with $\mu$ = 40 and $\sigma$ = 8.

**How many observation plots must be randomly selected and observed to be able to determine statistically significant changes on the level of 1 species and at the 5 % error level?**

```{r}
# z stays the same, sigma=8, so sigma squared 8**2, d = 1
get_n(z = 1.96, sigma_sq = 8**2, d=5)

```
Answer: we need 246 plots to be sure for d=1, level of precision d=5 reduced the amount of plots to 10. This way more feasible.



### Exercise 3
From previous studies it is known that a certain forest beetle usually attacks 20% of the trees. From unsystematic observations it is supposed that the share of attacked trees has increased from 20% to 25%.

**How many randomly selected trees have to be checked to justify this hypothesis with 5% error?**
```{r}
## ?? What is the probability function? since we have no sigma or mu. p(1-p)
## We can actually just adabpt the formula

get_n_p <- function(z, p, d) {
  n <- z**2 * p*(1-p) / d**2 
  return(n)
}

# is the change we want to detect, since it 0.20 to 0.25 d=0.05
# p is the percentage value we know, and we want to know how many trees we need to check for the assumed p=0.25
#get_n_p(z = 1.96, p=0.20, d=0.05)
get_n_p(z = 1.96, p=0.20, d=0.05)
```

Keep in mind. For moving objects these methods don't apply directly.



Adapt the formula above!

Define values for $z$, $p$, and $d$ and proceed with the calculation!

*End of seminar 8*