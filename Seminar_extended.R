

summary.extended<-function(d){
  
  quartiles<-quantile(d, c(0.25,0.75), na.rm=TRUE)
  mi<-min(d,na.rm=TRUE)
  ma<-max(d,na.rm=TRUE)
  rng<-abs(mi-ma)
  iqr<-abs(diff(quartiles))
  stdev<-sd(d, na.rm=TRUE)
  n<-length(d)
  se<-stdev/sqrt(n)
  
  results<-c(
    mi,
    quartiles[1], # 25% Quartil
    mean(d, na.rm=TRUE),
    median(d, na.rm=TRUE),
    quartiles[2],
    rng,
    iqr,
    ma,
    stdev,
    se,
    n
  )
  labels<-c(		"MIN",
              "25%Q",
              "MEAN",
              "MED",
              "75%Q",
              "RNG",
              "IQR",
              "MAX",
              "STDEV",
              "STDERR",
              "N"
  )
  df<-data.frame(row.names = labels, Param = round(results, 3))
  return(df)
}