---
title: "Analysis of DBH at Eberswalde Marteloscope"
author: "Ellen Gomes, Jan Rapczynski, Jennifer Wenzel, Christian Winkelmann"
date: "`r paste(Sys.Date())`"
#output:
#  pdf_document: default
output: html_notebook
---

*Analysis of Marteloscope Data for the Data Analysis Group work*

```{r, setup, include=FALSE}
#install.packages("dplyr")
#install.packages("knitr")
```

```{r}
library(dplyr)
```

```{r echo=FALSE, message=FALSE, warning=FALSE }
trees <- read.delim("/home/christian/Insync/christian.winkelmann@gmail.com/Google Drive/documents/Studium/FIT/courses/Environmental spatial data analysis - Wallor M3/projectwork/projectdata.tsv") ## make sure this file is available
# trees <- read.delim("/data/projectdata.tsv") ## make sure this file is available
head(trees)
```



**summarize counts all over**
```{r}
table(trees$Species)
```
```

trees %>%
group_by(Species) %>%
summarise_at(vars(DBH), list(name = mean))


```
**Count of Common pine per Quadrant**
```{r echo=FALSE, message=FALSE, warning=FALSE}
subset(trees, Species=='Common pine') %>% group_by(Quadrant) %>% tally()
```

**Count of Copper beech per Quadrant**
```{r echo=FALSE, message=FALSE, warning=FALSE}
subset(trees, Species=='Copper beech') %>% group_by(Quadrant) %>% tally()
```



```{r echo=FALSE, message=FALSE, warning=FALSE}

trees_1 <- trees %>% filter(Quadrant == '1' & ( Species == "Copper beech" | Species == "Common pine") )
trees_2 <- trees %>% filter(Quadrant == '2' & ( Species == "Copper beech" | Species == "Common pine") )
trees_3 <- trees %>% filter(Quadrant == '3' & ( Species == "Copper beech" | Species == "Common pine") )
trees_4 <- trees %>% filter(Quadrant == '4' & ( Species == "Copper beech" | Species == "Common pine") )
```

# ***Count of trees per quadrant***
```{r, include=FALSE}
nrow(trees_1)


```



**Calculate the Frequency**
***of all***
```{r}
hist(x= trees$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter of all trees", freq=TRUE)
```


```{r message=FALSE, warning=FALSE}

hist(x= trees_1$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter for two tree species on site 1", freq=TRUE)
```
```{r echo=FALSE, message=FALSE, warning=FALSE}

hist(x= subset(trees_1, Species== "Copper beech")$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter Copper beech on quadrant 1", freq=TRUE)
```

***of site 1 and only Common Pine***
```{r}
jpeg(file="images/Distribution of diameter for Common pine on quadrant 1.jpeg")
hist(x= subset(trees_1, Species== "Common pine")$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter for Common pine on quadrant 1", freq=TRUE, col="darkgreen")


dev.off()

```

```{r}
jpeg(file="images/Histogram_Common pine and Copper beech_q1.jpeg")

hist(trees_1$DBH, 
     breaks = 40,
     xlab = 'Height in cm',
     main = 'Common pine and Copper beech Q1',
     col = 'white')

hist(subset(trees_1, Species== "Common pine")$DBH, 
     breaks = 20,
     xlab = 'Height in cm',
     col = 'blue',
     add = T)

hist(subset(trees_1, Species== "Copper beech")$DBH, 
     breaks = 20,
     col = 'red', 
     add = T)
legend("topright", legend = c("Common pine", "Copper beech"), fill = c("red", "blue"))

dev.off()
```


**Calculate the average and mean**
```{R echo=FALSE, message=FALSE, warning=FALSE}
jpeg(file="images/boxplots_Common pine and _q1.jpeg")

trees$Quadrant <- as.factor(trees$Quadrant)
boxplot(DBH ~ Quadrant, data=subset(trees, Species== "Common pine"), col=c("blue", "green", "yellow", "red"), main = 'Common pine All Quadrants')
legend("topleft", legend = levels(trees$Quadrant), fill = c("blue", "green", "yellow", "red"))

dev.off()


```
```{R}
jpeg(file="images/boxplots_Copper beech and _q1.jpeg")

trees$Quadrant <- as.factor(trees$Quadrant)
boxplot(DBH ~ Quadrant, data=subset(trees, Species== "Copper beech"), col=c("blue", "green", "yellow", "red"), main = 'Copper beech All Quadrants')
legend("topleft", legend = levels(trees$Quadrant), fill = c("blue", "green", "yellow", "red"))
dev.off()
```


```{r include=FALSE}
## **Test first if the data is normally distributed**

## test if all the data is normally distributed
shapiro.test(trees$DBH)
```

Test if Copper beech in quadrant 1-4 is normally distributed. If p-value > 0.05 null hypothesis should be accepted that data is normally distributed.
```{r}
shapiro.test(subset(trees, Species== "Copper beech")$DBH) 

shapiro.test(subset(trees, Species== "Common pine")$DBH) 
```


Test if Copper beech in quadrant 1-4 is normally distributed. If p-value > 0.05 null hypothesis should be accepted that data is normally distributed.
```{r}
shapiro.test(subset(trees_1, Species== "Copper beech")$DBH)
shapiro.test(subset(trees_2, Species== "Copper beech")$DBH)
shapiro.test(subset(trees_3, Species== "Copper beech")$DBH)
shapiro.test(subset(trees_4, Species== "Copper beech")$DBH)
```

Test if Common pine in quadrant 1-4 is normally distributed. If p-value > 0.05 null hypothesis should be accepted that data is normally distributed.
```{r}
shapiro.test(subset(trees_1, Species== "Common pine")$DBH)
shapiro.test(subset(trees_2, Species== "Common pine")$DBH)
shapiro.test(subset(trees_3, Species== "Common pine")$DBH)
shapiro.test(subset(trees_4, Species== "Common pine")$DBH)
```
***Result***
Common pine on quadrant 1,4 clearly is normally distributed.
Common pine on quadrant 3 is not normally distributed and on quadrant 2 barely.




***Test for difference in dbh of species Common pine in quadrant 1 and 4, the other cannot be tested since they are not normally distributed***


```{r}
# Null Hypothesis: there is NO difference in the between quadrant 1 and quadrant 4
t.test(subset(trees_1, Species== "Common pine")$DBH, subset(trees_4, Species== "Common pine")$DBH, paired = FALSE, alternative = c("two.sided"))

#cat("p-value is: ", ttest_result$p.value, " so we accept the null hypothesis, the means are not different, because p-value is higher than 0.05 ")

```
