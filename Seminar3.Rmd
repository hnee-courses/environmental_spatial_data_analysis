---
title: "Descriptive statistics"
output: html_notebook
---
This notebook is intended for the support of students in the course *Environmental spatial data analysis*. The aim of this practical seminar is

+ to import external data into RStudio, to process and to save data,
+ to apply illustrative statistics (plots) and descriptive statistics,
+ to interpret statistical results and export them.

**Data sources**: data_seminar2.txt
**Extra Functions**: Seminar_extended.R


Setup Chunk to install dependencies
```{r, setup, include=FALSE}
install.packages("ggplot2")
```

```{r}
sessionInfo()
```





**Remark**: Everybody works on her/his personal directory.

## General settings
### Working directory
When starting a new session in RStudio it is recommended to define the working directory using `setwd()`. There, R scripts and R environments are locally saved (cf. Introduction to R and RStudio.pdf). If you are not sure about the data path type `getwd()` and the directory is shown. Copy the directory into the brackets of `setwd()`.

### Import data
Data provided for the seminar is saved in the file *data_seminar2.txt* and consists of a column *No*, *Area*, *Management*, *Height*, *BHD*, and *Grade*. Import the data from your local directory by using *Import Dataset* of the *Environment tab* or by using `read.delim()`.

**Adapt the data path to your individual one and do not forget to save your R script and environment at the end of the session**!
```{R}
data <- read.delim("data/data_seminar2.txt") ## make sure this file is available
head(data)
```
**Exercise 1**: Discuss the data types and meta-information (e.g. units)!

### Libraries
Libraries in R provide specific applications (e.g. model functions, plotting functions etc.) for different purposes. To get access to these applications it is necessary to first install and secondly load the libraries. To install use `install.packages(name)` or *Install* of the *Packages tab*, to load use `library(name)`. The following libraries are required for this seminar:
```{R}
library(ggplot2)
```

## Data exploration using illustrative statistics
### Histograms
A histogram shows the distribution of observed values of a variable of interest. According to lecture no. 3 it is differentiated between the absolute and relative frequency as well as the empirical density. In case of continuous variables observed values are assigned to classes. A histogram can show the class frequencies or the cumulative frequency of the sample.

**Exercise 2**: Work through the following examples for the variable *BHD* in centimeters [cm]. Consider all data entries. Here is some additional information about [graphical parameters in R](https://www.statmethods.net/advgraphs/parameters.html).

#### Example: Absolute frequency
```{R fig1}
hist(x= data$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter", freq=TRUE)
```
The interpretation of the above could be that it is mulimodal. https://en.wikipedia.org/wiki/Multimodal_distribution, so there are actually two modals, two classes.


#### Example: Density plot
```{R fig2}
hist(data$DBH, xlab="Tree diameter [cm]",
     main="Distribution of diameter", ylab="Density",
     freq=FALSE)
density <- density(data$DBH)
lines(density, col="blue")
```
#### Example: Probability plot
```{R fig3}
prob <- hist(data$DBH,  plot=FALSE)
prob$counts <- (prob$counts/sum(prob$counts)) # probability
plot(prob, main="Distribution of diameter" , ylab="Probability", xlab="Tree diameter [cm]", col="yellow")
```
#### Example: Cumulative plot
```{R fig4}
cum <- ecdf(data$DBH)    
plot(cum, main="Cumulative distribution", xlab="Tree diameter [cm]", col="blue")
```
**Exercise 3**: Select one of the histograms and visualize also the value distribution of the variable *Height* [m]. Discuss and interpret the results!


```{r fig33}
hist(data$Height, xlab="Tree height [m]",
     main="Distribution of height", ylab="Frequency",
     freq=TRUE)
#density <- density(data$Height)
#lines(density, col="blue")
```



### Boxplots
The box and whisker plot illustrates important statistical parameters of location and dispersion (cf. lecture no. 3).

**Exercise 4**: Work through the following examples for the variable *DBH* in [cm]. Proceed similar for the variable *Height* [m].

#### Example: Boxplot using all data
```{R fig5}
boxplot(data$DBH, ylab="Tree diameter [cm]", col="green")
```
#### Example: Boxplot per factor level *Area*
The two-peaked histograms suggest that there are differences between the continuous variables. We first examine the nominally scaled feature *Area*. Therefore, the feature *Area* needs to be transformed to data type *factor level* instead of *character*: 
```{R fig6}
data$Area <- as.factor(data$Area)

boxplot(DBH ~ Area, data=data, col=c("blue", "green"))

legend("topleft", legend = levels(data$Area), fill = c("blue", "green"))

## trying to add the mean too
#par(new=TRUE)
#means <- tapply(data$Area, data$Area,mean)
#points(means,col="red",pch=18)

```
**Exercise 5**: Create a similar boxplot with respect to the variables *Management* and *Grade*. Do the same for the continuous variable *Height* [m]. Discuss and interpret the results.

### summary.extended function
The R-script *Summary_extended.R* provides a function that includes all calculations for statistical parameters of location and dispersion. 

**Exercise 6**: Import the script into your local R session and run it. Afterwards, the function `summary.extended()` is part of your R environment. Apply the function to one of the metric variables of our data (see below). Compare the values with the corresponding boxplot.
```{R}
source("Seminar_extended.R") ## make sure this file is available
summary.extended(data$DBH)
```
**Remark**: In case you want to store the output of the command `summary.extended()` in your environment type `output<-summary.extended()`. 

In case you want to apply the function only to a subset of your data separated by another feature type for example `lausitz<-subset(data, Area=="LAUSITZ")`. 

In case you want to export a stored output from the environment to your local directory type `write.table(output, "~/your/data/path/output.txt", sep="\t", quote=F, row.names=TRUE)`.

*End of seminar 3*