---
title: "Statistical tests I"
output: html_notebook
---
This notebook is intended to support students in the course *Environmental spatial data analysis. The aim of this practical seminar is

+ to manifest skills in data handling and management using R
+ to select and apply appropriate statistical tests dependent on scientific question and data properties
+ to define hypothesis and interpret test results.

**Data sources**: data_seminar2.txt OR R environment of the seminar *Descriptive statistics*

**Remark**: Everybody works on her/his personal directory

## General settings
In seminar 3 we explored the data *data_seminar2.txt* by applying some illustrative and descriptive statistics. Open the respective R environment from seminar 3 to proceed with statistical testing. To open an existing R environment you may use the statement `load("file path")` or the *open folder* button of the *Environment* tab in the upper right window of RStudio.

## Starting with tests: t-test
In lecture 4 we learned that the t-test is applied to analyse samples for differences in the mean value: Either between a sample and a reference (= One-sample t-test) or between two samples. In case the two samples are paired the Paired-sample t-test is applied, in case the two samples are independent the Independent-sample t-test is applied.

In R, the respective statement is `t.test()` and it is specified in the arguments whether a paired or unpaired t-test is applied, whether it is one-sided or two-sided etc. Call `?t.test` to look for more information under *Help*.

**Example**: In the seminar *Descriptive statistic* we explored data of two pine tree stands located in two different areas (NOBRB, LAUSITZ). We know, that on average, pine tree stands of a similar age in Northern Germany show a DBH of 32.82 cm. Let's apply a t-test to proof if the pine stands of the two areas differ from this average. Therefore, it is necessary to split the data according to the area:
```{R}
nobrb <- subset(data, Area == "NOBRB")
lausitz <- subset(data, Area == "LAUSITZ")

t.test(nobrb$DBH, mu=32.82)
t.test(lausitz$DBH, mu=32.82)
```
**Exercise 1**: Interpret the results! What was the null hypothesis and alternative hypothesis?
TODO what was this??
p-value < 2.2e-16 is very small so we have to reject the null hypothesis. The null hypothesis states there is no difference in the means. The alternative hypothesis states there is a difference. This can be visualized. See in exercise 2.

**Exercise 2**: Remember the boxplots showing the value distribution of DBH [cm] for the two pine tree stands:
```{R fig1}
boxplot(DBH ~ Area, data=data, col=c("blue", "green"))
legend("topleft", legend = levels(data$Area), fill = c("blue", "green"))
```
Define the null and alternative hypothesis for a two-sample t-test that examines the differences of DBH [cm] measured at the two sample areas. Do we select a paired or unpaired t-test? Interpret the results!

**Exercise 3**: Examine for each area if there are differences in DBH [cm] related to the management strategy (1 and 2). Define the hypothesis and interpret the results.

```{r}
nobrb1 <- subset(nobrb, Management == 1) # 
nobrb2<- subset(nobrb, Management == 2) # subset north brandenburg again

# FIRST: is it a two sided or one sided test: two sided
# we apply the two sample t test, because we compare two samples. The t-test tests if the means differ.
# here if the diameter means

# SECOND: What are the hypothesis
# what is the null hypothesis: there is NO difference in the north brandenburg area
# alternative hypothesis? - there is difference

# THIRD: is it a paired or unpaired?
# Here it is independent not paired, because each sample consists different objects.

t.test(nobrb1$DBH, nobrb2$DBH, paired = FALSE, alternative = c("two.sided"))
# We don't have to provide mu because it is a two sided test

# RESULT: t = 0.31512, df = 37.952, p-value = 0.7544
# RESULT: null hypothesis can be accepted because p=0.7544. So there is no difference.

```
Repeat the same for lausitz
```{r}
lausitz1 <- subset(lausitz, Management == 1) # 
lausitz2<- subset(lausitz, Management == 2) # subset north lausitz again

# FIRST: is it a two sided or one sided test: two sided
# we apply the two sample t test, because we compare two samples. The t-test tests if the means differ.
# here if the diameter means

# SECOND: What are the hypothesis
# what is the null hypothesis: there is NO difference in the lausitz area
# alternative hypothesis? - there is difference

# THIRD: is it a paired or unpaired?
# Here it is independent not paired, because each sample consists different objects.

t.test(lausitz1$DBH, lausitz2$DBH, paired = FALSE, alternative = c("two.sided"))
# We don't have to provide mu because it is a two sided test

# RESULT: p-value = 0.9847
# RESULT: null hypothesis can be accepted because p=0.9847. So there is no difference.
## It should be lower than 0.05 to reject the null hypothesis
```

*End of seminar 4*