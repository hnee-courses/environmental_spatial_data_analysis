---
title: "Simple linear regression analysis"
output: html_notebook
---
This notebook is intended to support students in the course *Environmental spatial data analysis*. The aim of this practical seminar is:

+ to manifest skills in data handling and management using R,
+ to apply the required steps of a simple linear regression analysis,
+ to interpret the results of a simple linear regression analysis and apply the *lm* model.

**Data sources**: For this seminar the TXT file *projectdata.txt* is required. Please import it or open the R workspace / environment where the data is stored!
```{r}
projectdata <- read.delim("/home/christian/Insync/christian.winkelmann@gmail.com/Google Drive/documents/Studium/FIT/courses/Environmental spatial data analysis - Wallor M3/projectwork/projectdata.tsv") ## make sure

## TODO if you get problems here, convert Species to factor

```

**Remark**: Everybody works on her/his own directory.

## Starting with linear regression analysis: Graphical assessment
When imported or loaded the *projectdata* of the marteloscope site at Eberswalde, we can look for a relationship between the two **continuous** features *tree height* and *DBH*. Both features play an important role in so-called allometric tree models for the estimation of tree biomass and usually they are more or less strongly related with each other. First, we create a scatter plot with *DBH* on the x-axis and *tree height* on the y-axis:
#```{R fig1, fig.height=1.5, fig.width=3, out.width='50%'}
```{R fig1}

plot(projectdata$DBH, projectdata$Height, col=projectdata$Species, xlab = "DBH", ylab = "Tree height")
legend('bottomright', legend = levels(projectdata$Species), col = 1:6, cex = 0.8, pch = 1)
```
**Exercise 1: Interpret the graph!**
With increasing diameter, the height increases to. 


## Continuing with linar regression analysis: The correlation coefficient
Following the interpretation of the scatter plot above, it is necessary to examine potential relationships between *tree height* and *DBH* specifically for each tree species. From the presentations of the group work conducted during the course, we know already, that the variable values might not always be normally distributed. Therefore, we calculate the correlation coefficient according to Spearman to get a first impression on the strength of the (linear) relationship. Spearman's correlation coefficient is applied for non-normally distributed continuous values or categorical values of discrete scale. In this example we subset the data for the species *Common spruce*:
```{R}
spruce <- subset(projectdata, Species == "Common spruce")
head(spruce)
```
```{R}
cor(spruce$DBH, spruce$Height, method = c("spearman"))
```
**Exercise 2: Interpret the correlation coefficient! Which result is produced when applying Pearson's correlation?**
The result is: 0.968, that means we have a very high correlation. It could be up to -1 as well. Then the values are highly anti-correlated.

## Continuing with linear regression analysis: The linear regression model
From the calculated correlation coefficient it is obvious that there is a strong relationship between measured *tree height* and measured *DBH* for the tree species *Common spruce*. Due to the fact that Spearman's correlation reflects monotone relations we consider first a simple **linear** regression analysis. The corresponding function in R is `lm()`. Have a look at the required arguments by calling the help `?lm`. By applying the R function `lm()` a corresponding object is created in the R workspace:
```{R}
linear_model <- lm(Height ~ DBH, data = spruce)
summary(linear_model)
```
**Exercise 3: Interpret the results with respect to the model equation and the quality of the linear model! Review your R workspace: What is stored in the object *linear_model*?**
ANSWER:
residuals: deviation of measured values to modeled values.
Minimal underestimation is -1.6929m and maximal overestimation is 2.26m

How many coefficients to we have?
two: y = a + b * x
a = 2.52975
b = 0.84843, where as x=DBH
```{r}
linear_model$coefficients
```

Of course, it is a good idea to plot the results of the linear regression model to check if it is really appropriate. Therefore, a sequence of values is created reflecting the independent variable *x = DBH*. Secondly, and based on this sequence, the predicted values are calculated by applying the linear regression model *linear_model*:
```{R fig2,}
x <- seq(min(spruce$DBH), max(spruce$DBH), by = 0.1)
predict <- coef(linear_model)[1] + coef(linear_model)[2]*x

plot(spruce$DBH, spruce$Height, ylab="Height [m]", xlab="Diameter [cm]")
lines(x, predict, col="red")
abline(h=c(0,10,20,30), lty="dashed", col="grey")
abline(v=c(10,20,30,40,50), lty="dashed", col="grey")
```
**Exercise 4: We achieved a high coefficient of determination $R^2$ for the linear regression model. Interpreting the regression plot above, are you satisfied with the result?**

We see higher deviations between modeled and observed values for *tree height* at lower and higher *DBH* values. The dots in our plot(s) describing the relationship between the two variables describe rather a curve than a line. Due to the curve's progress we try a logarithmic model instead following the steps described above:

**Step 1: Model set-up**
```{R}
log_model <- lm(Height ~ log(DBH), data = spruce)
summary(log_model)
```
Coeficient of determination is higher ( 0.9798 ) when DBH is logarithmized that it was with the linear function which was 0.9439 for the linear model.
see this if you wonder about the ~ (Tilde symbol) https://statisticsglobe.com/use-of-tilde-in-r

**Step 2: Create a vector for the predictions**
```{R}
log_predict <- coef(log_model)[1] + coef(log_model)[2]*log(x)
```
**Step 3: Visualize the outcome of the logarithmic model**
```{R fig3}
plot(spruce$DBH, spruce$Height, ylab="Height [m]", xlab="Diameter [cm]")
lines(x, log_predict, col="red")
abline(h=c(0,10,20,30), lty="dashed", col="grey")
abline(v=c(10,20,30,40,50), lty="dashed", col="grey")
```

*End of seminar 7*